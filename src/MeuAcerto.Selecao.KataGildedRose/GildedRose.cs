﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()

        {
    
            foreach (Item item in Itens){
                
                switch (item.Nome)
                {

                    // Como de acordo ao enunciado nao é permitido alterar o comportamento do Item com classes, implementei as regras de especialização do Item no switch mesmo

                    case "Sulfuras, a Mão de Ragnaros": break;                    

                    case "Ingressos para o concerto do TAFKAL80ETC":
                        {
                            if (item.PrazoValidade <= 5) aplicarQualidade(item, 3); // Se menor ou igual a 5 dias, qualidade aumenta de 3 em 3

                            else if (item.PrazoValidade <= 10) aplicarQualidade(item, 2); // Se menor ou igual a 10 dias, qualidade aumenta de 2 em 2

                            else aplicarQualidade(item, 1);   // Para ingressos com mais de 10 dias restants para vencer, qualidade aumenta de 1 em 1


                            if (item.PrazoValidade <= 0) item.Qualidade = 0;  // Ingresso vencido tera qualidade 10
                        }
                        break;
                    case "Queijo Brie Envelhecido": aplicarQualidade(item, 1, 1); // Como não foi especificado o comportamento do Queijo Brie após o vencimento, considerei que sua qualidade
                        break;                                                    // segue aumentando de 1 em 1, evitando que sua qualidade vá aumentando de 2 em 2 após vencimento

                    case "Bolo de Mana Conjurado": aplicarQualidade(item, -2);  // Item conjurado diminui qualidade duas vezes mais rapido que os outros
                        break;

                    default: aplicarQualidade(item);
                        break;
                }
            }
        }

        public void aplicarQualidade(Item item, int velocidade = -1, int velocidadeVencido = 2, int MAX_QUALIDADE = 50, int MIN_QUALIDADE = 0)
        {

            if (item.PrazoValidade <= 0 ) velocidade *= velocidadeVencido;  // Se item esta vencido, a velocidade de perder qualidade sera multiplicada pelo fator

            if ((item.Qualidade + velocidade) < MIN_QUALIDADE) item.Qualidade = MIN_QUALIDADE;  //Se o decrescimo levar sua qualidade para abaixo da qualidade minima, assume valor minime

            else if ((item.Qualidade + velocidade) > MAX_QUALIDADE) item.Qualidade = MAX_QUALIDADE; //Se o decrescimo levar sua qualidade para abaixo da qualidade minima, assume valor minimo
            
            else item.Qualidade += velocidade; // Decresce a qualidade

            item.PrazoValidade--;   

        }
    }
}
